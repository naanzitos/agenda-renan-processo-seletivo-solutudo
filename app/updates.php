<!DOCTYPE html>
<html>
    <head> 
        <?php require('head.php'); ?>
            
        <!-- CHECK LOGIN -->
        <?php require('validation.php'); ?>
    </head>
    <body>
        <?php require('menu.php'); ?>
        <div class='h1List'>Notas</div>
        <div id='updates'>
            <h1>Funcionalidades v1.0:</h1><br><br>
            
            <p> * Todos os usuários podem efetuar login no sistema</p><br><br>
            <p> * O gerente pode visualizar todos os cadastros</p><br><br>
            <p> * Todos os usuários podem inserir, editar, e excluir clientes</p><br><br>
            <p> * Processos de edição e exclusão estão restritos aos registros pertencentes ao respectivo usuário logado, que cadastrou o cliente</p><br><br>
            <p> * O gerente pode inserir, editar e excluir registros, porém, só ele pode editá-los depois</p><br><br><br><br>
            
            <h1>OBSERVAÇÕES:</h1><br><br>
            
            <p> * Não foi utilizado nenhum framework back-end, nem front-end</p><br><br>
        </div>
        <div id='bottom'>
            <center><h1>Agenda Renan 2015</h1></center>
        </div>
    </body>
</html>