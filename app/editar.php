<!DOCTYPE html>
<html>
    <head> 
        <?php require('head.php'); ?>
        
        <!-- CHECK LOGIN -->
        <?php require('validation.php'); ?>
    </head>
    <body>
       <?php require('menu.php'); ?>
       <?php 
            
            require('db.php');

            $id = $_GET['action'];   
            $sql =  "SELECT * FROM clientes WHERE id ='$id'";
            $query = mysqli_query($link, $sql);
            $row = mysqli_fetch_assoc($query);

            if($_SESSION['nivel'] != 'gerente'){
                if($row['editor'] != $_SESSION['nickname']){
                    header('Location: index.php');
                }   
            }
        
        ?>
        <div id = 'contentUpdate'>
            <div class='signUpBox'>
                <h1>Alterar Cadastro de Cliente</h1>
                <form action='alterarcadastro.php' method='post'><br>
                    <h1>Nome:</h1>
                    <input type='text' autocomplete='off' value='<?php echo $row['nome']; ?>' required="true" name='nome' class='alterarInput'>
					<h1>Email:</h1>
                    <input type='text' autocomplete='off' value='<?php echo $row['email']; ?>' required="true" name='email' class='alterarInput'>
                    <h1>Telefone:</h1>
                    <input type='text' autocomplete='off' value='<?php echo $row['telefone']; ?>' required="true" name='telefone' class='alterarInput tel'>
                    <h1>Endereço:</h1>
                    <input type='text' autocomplete='off' value='<?php echo $row['endereco']; ?>' required="true" name='endereco' class='alterarInput endereco'>
                    <input type='text' autocomplete='off' value='<?php echo $row['numero']; ?>' required="true" name='numero' placeholder='Nº' class='alterarInput num'>
                    <br><br><br><br>
                    <h1>Data de Nascimento:</h1>
                    <input type='date' autocomplete='off' value='<?php echo $row['data_nasc']; ?>' name='data_nasc' class='alterarInput data'><br>
                    <input type='hidden' name='id' value='<?php echo $id; ?>'>
                    <input type='submit' value='Alterar Cadastro' class='buttonUpdt'>
                </form>
            </div>
        </div>
        <div id='bottom'>
            <center><h1>Agenda Renan 2015</h1></center>
        </div>
    </body>