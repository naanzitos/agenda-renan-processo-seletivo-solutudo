<!DOCTYPE html>
<html>
    <head> 
        <?php require('head.php'); ?>
            
        <!-- CHECK LOGIN -->
        <?php require('validation.php'); ?>
    </head>
    <body>
        <?php require('menu.php'); ?>
            <div class='h1List'>Lista de Clientes</div>
            <div id='clientList'>
                <div class='listTitle'>
                    <div class='nameTitle'>
                        Nome do Cliente
                    </div>
                    <div class='emailTitle'>
                        Email
                    </div>
                    <div class='telTitle'>
                        Telefone
                    </div>
                    <div class='nascTitle'>
                        Data de Nascimento
                    </div>
                </div>
                <?php 

                    require('db.php');

                    $sql =  "SELECT * FROM clientes ORDER BY id ASC";
                    $query = mysqli_query($link, $sql);

                    while ($row = mysqli_fetch_assoc($query)){

                    echo "<div class='clientRow'><a href='cliente.php?action=".$row['id']."'>
                            <div class='rowName'>
                                ".$row['nome']."
                             </div>
                             <div class='emailText'>
                                ".$row['email']."
                             </div>
                             <div class='telText'>
                                ".$row['telefone']."
                             </div>
                             <div class='dataNascText'>
                                ".$row['data_nasc']."
                             </div>
                             <a/>";

                        if ($row['editor'] == $_SESSION['nickname'] || $_SESSION['nivel'] == 'gerente'){   

                            echo"
                                 <div class='editButton'>
                                    <a href='editar.php?action=".$row['id']."'>
                                        <img src='img/pencil.png' width='25' height='25'>
                                    </a>
                                 </div>
                                  <div class='deleteButton'>
                                    <a href='deleterow.php?action=".$row['id']."'>
                                        <img src='img/delete.png' width='30' height='30'>
                                    </a>
                                 </div>
                             ";

                        } echo"</div>";         
                     } 
                ?>  
        </div>
        <div id='bottom'>
            <center><h1>Agenda Renan 2015</h1></center>
        </div>
    </body>
</html>