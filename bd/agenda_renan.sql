-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tempo de Geração: 09/04/2015 às 03h06min
-- Versão do Servidor: 5.5.20
-- Versão do PHP: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `agenda_renan`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `numero` varchar(50) NOT NULL,
  `data_nasc` date NOT NULL,
  `editor` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `nome`, `email`, `telefone`, `endereco`, `numero`, `data_nasc`, `editor`) VALUES
(16, 'Renan Nogueira', 'naanzitos@gmail.com', '(14) 99708-9832', 'Rua Gerson Giandoni', '132', '1996-06-13', 'usuario3'),
(17, 'Brenda Correia Alves', 'BrendaCorreiaAlves@dayrep.com', '(41) 8469-4592', 'Rua dos Hobbits', '42', '1990-01-02', 'usuario3'),
(18, 'Diego Lima Cunha', 'DiegoLimaCunha@jourrapide.com', '(24) 9234-3525', 'Rua dos Hobbits', '42', '1990-01-03', 'usuario3'),
(19, 'Pedro Santos Almeida', 'PedroSantosAlmeida@dayrep.com', '(12) 5179-9505', 'Rua Hogwarts', '4242', '1990-01-04', 'usuario3'),
(20, 'Igor Azevedo Ribeiro', 'IgorAzevedoRibeiro@teleworm.us', '(14) 8371-6051', 'Rua Doida ', '4242', '1990-01-05', 'usuario3'),
(21, 'Julia Lima Araujo', 'JuliaLimaAraujo@dayrep.com', '(65) 6513-9711', 'Rua Aleatória', '45452', '1990-01-06', 'usuario1'),
(22, 'Kauã Rodrigues Costa', 'KauaRodriguesCosta@teleworm.us', '(11) 3471-5488', 'Blá blá blá', '42242', '1990-01-07', 'usuario1'),
(23, 'Lucas Costa Martins', 'LucasCostaMartins@dayrep.com', '(55) 2772-7911', 'HUE HUE HUE HUE', '8754', '2000-05-06', 'usuario1'),
(24, 'Rodrigo Araujo Gomes', 'RodrigoAraujoGomes@dayrep.com', '(83) 6851-5592', 'asdhfuiawyeuiruiasdhfjkhadjkf', '145456', '2001-04-05', 'usuario1'),
(25, 'Diogo Azevedo Almeida', 'DiogoAzevedoAlmeida@jourrapide.com', '(41) 6703-5452', 'asodhfadfuioauweioasiodufioasd', '23465', '1504-07-09', 'usuario1'),
(26, 'Rodrigo Pereira Oliveira', 'RodrigoPereiraOliveira@dayrep.com', '(42) 5123-5333', 'asdfçakjsdflçasdf', '65456', '2004-05-06', 'usuario1'),
(27, 'Thaís Martins Silva', 'ThaisMartinsSilva@rhyta.com', '(21) 3602-3667', '545645645645', '13115', '4542-05-06', 'usuario2'),
(28, 'Joao Martins Silva', 'JoaoMartinsSilva@dayrep.com', '(21) 3446-9795', 'auahuhauhuahua', '42', '2002-03-31', 'usuario2'),
(29, 'Daniel Rodrigues Barbosa', 'DanielRodriguesBarbosa@jourrapide.com', '(81) 2113-4213', 'Rua Hogwarts', '42', '1950-02-05', 'usuario2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `nivel_acesso` varchar(50) NOT NULL,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `senha`, `nivel_acesso`, `nome`) VALUES
(3, 'usuario1', 'usuario', 'usuario', 'Tony Stark'),
(4, 'usuario2', 'usuario', 'usuario', 'George R. R. Martin'),
(5, 'usuario3', 'usuario', 'usuario', 'Jack Sparrow'),
(6, 'usuario4', 'usuario', 'usuario', 'Gandalf'),
(7, 'usuario5', 'usuario', 'usuario', 'Darth Vader'),
(8, 'gerente', 'gerente', 'gerente', 'Goku');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
