**************************
      AGENDA_RENAN
	  2015
    Processo Seletivo
	SOLUTUDO
**************************

Funcionalidades v1.0:


* Todos os usu�rios podem efetuar login no sistema


* O gerente pode visualizar todos os cadastros


* Todos os usu�rios podem inserir, editar, e excluir clientes


* Processos de edi��o e exclus�o est�o restritos aos registros pertencentes ao respectivo usu�rio logado, que cadastrou o cliente


* O gerente pode inserir, editar e excluir registros, por�m, s� ele pode edit�-los depois



OBSERVA��ES:


* N�o foi utilizado nenhum framework back-end, nem front-end

* O banco de dados "agenda_renan" est� no diret�rio ../bd/agenda_renan.sql



------------------
Lista de usu�rios
------------------

Nickname: usuario1
Senha: usuario

Nickname: usuario2
Senha: usuario

Nickname: usuario3
Senha: usuario

Nickname: usuario4
Senha: usuario

Nickname: usuario5
Senha: usuario

------------------
     Gerente
------------------

Nickname: gerente
Senha: gerente
