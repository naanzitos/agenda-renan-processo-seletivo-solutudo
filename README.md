#  AGENDA_RENAN #



## Funcionalidades v1.0: ##


* Todos os usuários podem efetuar login no sistema


* O gerente pode visualizar todos os cadastros


* Todos os usuários podem inserir, editar, e excluir clientes


* Processos de edição e exclusão estão restritos aos registros pertencentes ao respectivo usuário logado, que cadastrou o cliente


* O gerente pode inserir, editar e excluir registros, porém, só ele pode editá-los depois



## OBSERVAÇÕES: ##


* Não foi utilizado nenhum framework back-end, nem front-end

* O banco de dados "agenda_renan" está no diretório ../bd/agenda_renan.sql



## Lista de usuários ##

* Nickname: usuario1
* Senha: usuario


* Nickname: usuario2
* Senha: usuario


* Nickname: usuario3
* Senha: usuario


* Nickname: usuario4
* Senha: usuario


* Nickname: usuario5
* Senha: usuario


##  Gerente  ##


* Nickname: gerente
* Senha: gerente